# Generated from http_parser.rb-0.6.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name http_parser.rb

Name: rubygem-%{gem_name}
Version: 0.6.0
Release: 1%{?dist}
Summary: Simple callback-based HTTP request/response parser
Group: Development/Languages
License: MIT
URL: http://github.com/tmm1/http_parser.rb
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel
# BuildRequires: rubygem(rake-compiler) >= 0.7.9
# BuildRequires: rubygem(rspec) >= 2.0.1
# BuildRequires: rubygem(json) >= 1.4.6
# BuildRequires: rubygem(benchmark_suite)
# BuildRequires: rubygem(ffi)
# BuildRequires: rubygem(yajl-ruby) >= 0.8.1
Provides: rubygem(%{gem_name}) = %{version}

%description
Ruby bindings to http://github.com/ry/http-parser and
http://github.com/a2800276/http-parser.java.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{gem_extdir_mri}/lib
# TODO: move the extensions
mv %{buildroot}%{gem_instdir}/lib/shared_object.so %{buildroot}%{gem_extdir_mri}/lib/

# Prevent dangling symlink in -debuginfo (rhbz#878863).
rm -rf %{buildroot}%{gem_instdir}/ext/



# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.6.0-1
- Initial package

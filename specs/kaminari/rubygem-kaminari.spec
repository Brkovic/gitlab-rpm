# Generated from kaminari-0.15.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name kaminari

Name: rubygem-%{gem_name}
Version: 0.15.1
Release: 1%{?dist}
Summary: A pagination engine plugin for Rails 3+ and other modern frameworks
Group: Development/Languages
License: MIT
URL: https://github.com/amatsuda/kaminari
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(activesupport) >= 3.0.0
Requires: rubygem(actionpack) >= 3.0.0
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(tzinfo)
# BuildRequires: rubygem(rspec)
# BuildRequires: rubygem(rr)
# BuildRequires: rubygem(capybara) >= 1.0
# BuildRequires: rubygem(database_cleaner) => 1.2.0
# BuildRequires: rubygem(database_cleaner) < 1.3
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Kaminari is a Scope & Engine based, clean, powerful, agnostic, customizable
and sophisticated paginator for Rails 3+.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.rdoc

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.15.1-1
- Initial package

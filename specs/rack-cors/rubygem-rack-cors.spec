# Generated from rack-cors-0.2.9.gem by gem2rpm -*- rpm-spec -*-
%global gem_name rack-cors

Name: rubygem-%{gem_name}
Version: 0.2.9
Release: 1%{?dist}
Summary: Middleware for enabling Cross-Origin Resource Sharing in Rack apps
Group: Development/Languages
License: MIT
URL: http://github.com/cyu/rack-cors
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(shoulda)
# BuildRequires: rubygem(mocha) >= 0.14.0
# BuildRequires: rubygem(rack-test)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Middleware that will make Rack-based apps CORS compatible.  Read more here:
http://blog.sourcebender.com/2010/06/09/introducin-rack-cors.html.  Fork the
project here: http://github.com/cyu/rack-cors.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.2.9-1
- Initial package

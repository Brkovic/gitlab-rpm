# Generated from redis-rails-4.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name redis-rails

Name: rubygem-%{gem_name}
Version: 4.0.0
Release: 1%{?dist}
Summary: Redis for Ruby on Rails
Group: Development/Languages
License: 
URL: http://redis-store.org/redis-rails
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(redis-store) => 1.1.0
Requires: rubygem(redis-store) < 1.2
Requires: rubygem(redis-activesupport) => 4
Requires: rubygem(redis-activesupport) < 5
Requires: rubygem(redis-actionpack) => 4
Requires: rubygem(redis-actionpack) < 5
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
# BuildRequires: rubygem(mocha) => 0.14.0
# BuildRequires: rubygem(mocha) < 0.15
# BuildRequires: rubygem(minitest) => 4.2
# BuildRequires: rubygem(minitest) < 5
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Redis for Ruby on Rails.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 4.0.0-1
- Initial package

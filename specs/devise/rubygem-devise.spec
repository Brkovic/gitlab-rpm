# Generated from devise-3.2.4.gem by gem2rpm -*- rpm-spec -*-
%global gem_name devise

Name: rubygem-%{gem_name}
Version: 3.2.4
Release: 1%{?dist}
Summary: Flexible authentication solution for Rails with Warden
Group: Development/Languages
License: MIT
URL: https://github.com/plataformatec/devise
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(warden) => 1.2.3
Requires: rubygem(warden) < 1.3
Requires: rubygem(orm_adapter) => 0.1
Requires: rubygem(orm_adapter) < 1
Requires: rubygem(bcrypt) => 3.0
Requires: rubygem(bcrypt) < 4
Requires: rubygem(thread_safe) => 0.1
Requires: rubygem(thread_safe) < 1
Requires: rubygem(railties) >= 3.2.6
Requires: rubygem(railties) < 5
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Flexible authentication solution for Rails with Warden.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.2.4-1
- Initial package

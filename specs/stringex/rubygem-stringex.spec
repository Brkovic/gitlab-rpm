# Generated from stringex-2.5.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name stringex

Name: rubygem-%{gem_name}
Version: 2.5.2
Release: 1%{?dist}
Summary: Some [hopefully] useful extensions to Ruby's String class
Group: Development/Languages
License: MIT
URL: http://github.com/rsl/stringex
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
# BuildRequires: rubygem(jeweler) = 2.0.1
# BuildRequires: rubygem(travis-lint) = 1.7.0
# BuildRequires: rubygem(RedCloth) = 4.2.9
# BuildRequires: rubygem(sqlite3) = 1.3.7
# BuildRequires: rubygem(activerecord) = 4.0.3
# BuildRequires: rubygem(i18n) = 0.6.9
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Some [hopefully] useful extensions to Ruby's String class. Stringex is made up
of three libraries: ActsAsUrl [permalink solution with better character
translation], Unidecoder [Unicode to ASCII transliteration], and
StringExtensions [miscellaneous helper methods for the String class].


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/MIT-LICENSE
%doc %{gem_instdir}/README.rdoc

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 2.5.2-1
- Initial package

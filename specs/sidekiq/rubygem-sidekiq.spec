# Generated from sidekiq-3.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name sidekiq

Name: rubygem-%{gem_name}
Version: 3.0.0
Release: 1%{?dist}
Summary: Simple, efficient background processing for Ruby
Group: Development/Languages
License: LGPL-3.0
URL: http://sidekiq.org
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(redis) >= 3.0.6
Requires: rubygem(redis-namespace) >= 1.3.1
Requires: rubygem(connection_pool) >= 2.0.0
Requires: rubygem(celluloid) >= 0.15.2
Requires: rubygem(json)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(sinatra)
# BuildRequires: rubygem(minitest) => 4.2
# BuildRequires: rubygem(minitest) < 5
# BuildRequires: rubygem(rails) >= 4.0.0
# BuildRequires: rubygem(coveralls)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Simple, efficient background processing for Ruby.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{_bindir}/sidekiq
%{_bindir}/sidekiqctl
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.0.0-1
- Initial package

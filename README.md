## GitLab Packaging

This is a repository with all the SPEC files needed and are missing from the Fedora/EPEL repos. You can read more info in the [Fedora wiki][gitlab-fedora-wiki].

[gitlab-fedora-wiki]: https://fedoraproject.org/wiki/GitLab "GitLab Fedora wiki"
